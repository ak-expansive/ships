#!/bin/bash

IMGO_DEV_POLL_INTERVAL=${IMGO_DEV_POLL_INTERVAL:-5}

cp -r /go/src/app-dev/. /go/src/app-build/

find /go/src/app-dev/ -type f | xargs cat | base64 > /tmp/dev_files_hash.prev.txt

while true
do 
    find /go/src/app-dev/ -type f | xargs cat | base64 > /tmp/dev_files_hash.txt
		CHANGES=$(diff /tmp/dev_files_hash.prev.txt /tmp/dev_files_hash.txt | wc -l)

		if [ "$CHANGES" != "0" ]; then
			rm -rf /go/src/app-build/*
			cp -r /go/src/app-dev/. /go/src/app-build/
			echo "File changes found, copying /go/src/app-dev/ to /go/src/app-build" > /dev/stdout
		fi

		cat /tmp/dev_files_hash.txt > /tmp/dev_files_hash.prev.txt
    sleep $IMGO_DEV_POLL_INTERVAL
done

---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    name: web
  name: web
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      name: web
  template:
    metadata:
      labels:
        name: web
      name: web
    spec:
      imagePullSecrets:
      - name: gitlabpullsecret
      {{- if eq .dev_mode "true" }}
      volumes:
      - name: app-volume
        hostPath:
          path: {{ .host_path }}/src
      {{- end }}
      containers:
      {{- if eq .dev_mode "true" }}
      - image: "registry.gitlab.com/ak-expansive/ships/dev:latest"
      {{- else }}
      - image: "registry.gitlab.com/ak-expansive/ships:latest"
      {{- end}}
        imagePullPolicy: IfNotPresent
        name: ships
        {{- if eq .dev_mode "true" }}
        workingDir: "/go/src/app-dev"
        {{- end }}
        volumeMounts:
        {{- if eq .dev_mode "true" }}
        - name: app-volume
          mountPath: "/go/src/app-dev"
        {{- end }}
        ports:
        - containerPort: 8080
        env:
        - name: SHIPS_PORT
          value: "8080"
        - name: SHIPS_DB_PORT
          value: "5432"
        - name: SHIPS_DB_HOST
          value: "rdb-postgresql.expansive--shared.svc.cluster.local"
        - name: SHIPS_DB_USER
          value: "ships"
        - name: SHIPS_DB_PASSWORD
          value: "ships-svc"
        - name: SHIPS_DB_SCHEMA
          value: "ships"
        - name: SHIPS_DB_SSL_ENABLED
          value: "false"
        resources: ~

PROD_IMG_NAME ?= ships:prod
DEV_IMG_NAME ?= ships:dev

K8S_PROD_IMG_NAME ?= registry.gitlab.com/ak-expansive/ships:latest
K8S_DEV_IMG_NAME ?= registry.gitlab.com/ak-expansive/ships/dev:latest

.PHONY: build-prod
build-prod:
	docker build --target basebuild -t ${PROD_IMG_NAME} .

.PHONY: build-dev
build-dev:
	docker build --target dev -t ${DEV_IMG_NAME} .

# When we wanna build it straight into minikube docker engine
build-k8s-dev:
	docker build --target dev -t ${K8S_DEV_IMG_NAME} .

build-k8s-prod:
	docker build --target basebuild -t ${K8S_PROD_IMG_NAME} .
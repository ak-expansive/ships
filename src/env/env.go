package env

import (
	"errors"
	"os"
	"strconv"
	"log"
)


type validatorFunc func(string) error

// PortEnvKey ...
const PortEnvKey = "SHIPS_PORT"

const DBHostEnvKey = "SHIPS_DB_HOST"

const DBPortEnvKey = "SHIPS_DB_PORT"

const DBUserEnvKey = "SHIPS_DB_USER"

const DBPasswordEnvKey = "SHIPS_DB_PASSWORD"

const DBSchemaEnvKey = "SHIPS_DB_SCHEMA"

const DBEnableSSLEnvKey = "SHIPS_DB_SSL_ENABLED"

func portValidator (value string) error {
	valueAsInt, err := strconv.Atoi(value)

	if err != nil {
		return err
	}

	if valueAsInt < 0 || valueAsInt > 65535 {
		return errors.New("should be between 0 - 65535")
	}

	return nil
}

func notEmptyValidator(value string) error {
	if len(value) == 0 {
		return errors.New("cannot be empty")
	}

	return nil
}

func boolValidator(value string) error {
	switch value {
		case
			"true",
			"false":
			return nil
	}

	return errors.New("not a valid bool value, must be 'true' or 'false'")
}

var requiredEnvVars = map[string]validatorFunc{
	PortEnvKey: portValidator,
	DBHostEnvKey: notEmptyValidator,
	DBPortEnvKey: portValidator,
	DBUserEnvKey: notEmptyValidator,
	DBPasswordEnvKey: notEmptyValidator,
	DBSchemaEnvKey: notEmptyValidator,
	DBEnableSSLEnvKey: boolValidator,
}

func getErrors() map[string]string {
	errors := make(map[string]string)
	for varName, validator := range requiredEnvVars {

		value, ok := os.LookupEnv(varName)

		if !ok {
			errors[varName] = "Not set."
			continue
		}

		validationError := validator(value)

		if validationError != nil {
			errors[varName] = validationError.Error()
		}
	}

	return errors
}

// Get ...
func GetEnv(envVar string) (string, error) {
	value, exists := os.LookupEnv(envVar)

	if !exists {
		return "", errors.New(envVar + " not found.")
	}

	return value, requiredEnvVars[envVar](value)
}

// Validate ...
func ValidateEnv(show bool) error {

	validationErrors := getErrors()

	if len(validationErrors) == 0 {
		if show {
			for name := range requiredEnvVars {
				log.Printf("%s: %s\n", name, os.Getenv(name))
			}
		}

		return nil
	}

	for name, value := range validationErrors {
		log.Printf("Invalid value for %s, error: %s\n", name, value)
	}

	return errors.New("invalid environment configuration")
}

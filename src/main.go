package main

import (
	"log"
	"fmt"
	"os"
	"net/http"
	"github.com/gorilla/mux"
	"github.com/urfave/cli/v2"
	"github.com/gorilla/handlers"
	"gitlab.com/ak-expansive/ships/env"
	"gitlab.com/ak-expansive/ships/db"
)

func runServer(c *cli.Context) error {
	err := env.ValidateEnv(false)

	if err != nil {
		return err
	}

	r := mux.NewRouter()

	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Ships is running.")
	})

	log.Println("Server listening!")

	// We've already validated the env...shouldn't need to check for an error here...
	port, _ := env.GetEnv(env.PortEnvKey)

	handler := handlers.RecoveryHandler()(handlers.CombinedLoggingHandler(os.Stdout, r))

	return http.ListenAndServe(":" + port, handler)
}

func main() {

	app := &cli.App{
    Name: "serve",
		Usage: "Run the imgo server",
		Commands: []*cli.Command{
      {
        Name:    "serve",
        Usage:   "Run the imgo server",
        Action:  runServer,
			},
			{
				Name: "env",
				Usage: "Validate and print the env vars used for configuration",
				Action: func(c *cli.Context) error {
					return env.ValidateEnv(true)
				},
			},
			{
				Name: "test-db-conn",
				Usage: "Test that the app can connect to the Database.",
				Action: func(c *cli.Context) error {
					db, err := db.NewConn()

					if err != nil {
						return err
					}

					defer db.Close()

					log.Print("successfully connected to database")

					return nil
				},
			},
    },
  }

	err := app.Run(os.Args)

  if err != nil {
    log.Fatal(err)
	}
}

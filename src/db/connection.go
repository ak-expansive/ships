package db

import (
	"fmt"
	"log"
	"strconv"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/ak-expansive/ships/env"
)

const DBDriver = "postgres"

func buildConnectionStringFromEnv() string {
	err := env.ValidateEnv(false)
	
	if err != nil {
		log.Panic(err)
	}

	// We've just validated they exist so we can ignore the error; they can't be empty
	// They can onlly be 'bad' values which we'll find out when we try to connect
	host, _ := env.GetEnv(env.DBHostEnvKey)
	port, _ := env.GetEnv(env.DBPortEnvKey)
	user, _ := env.GetEnv(env.DBUserEnvKey)
	password, _ := env.GetEnv(env.DBPasswordEnvKey)
	schema, _ := env.GetEnv(env.DBSchemaEnvKey)
	SSLEnabledString, _ := env.GetEnv(env.DBEnableSSLEnvKey)
	enableSSL, _ := strconv.ParseBool(SSLEnabledString)
	sslModeValue := "enable"

	if ! enableSSL {
		sslModeValue = "disable"
	}

	return fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		host,
		port,
		user,
		schema,
		password,
		sslModeValue,
	)
}

func NewConn() (* gorm.DB, error) {
	connString := buildConnectionStringFromEnv()
	db, err := gorm.Open(DBDriver, connString)

	return db, err
}
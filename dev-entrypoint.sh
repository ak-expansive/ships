#!/bin/bash

/copy-changes.sh &

mkdir /go/src/app-build
cd /go/src/app-build

CompileDaemon --build="go build -o /go/bin/ships" --command="ships serve"